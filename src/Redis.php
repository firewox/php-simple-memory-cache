<?php


namespace Firewox\SimpleMemoryCache;


use Firewox\SimpleMemoryCache\Exceptions\InvalidDictionaryArgumentException;
use Firewox\SimpleMemoryCache\Exceptions\InvalidKeyTypeArgumentException;
use Predis\Client;
use Psr\SimpleCache\CacheInterface;

class Redis implements CacheInterface
{

  const STATUS_OK = 'OK';

  private $inst;
  private $_prefix;

  /**
   * Redis constructor.
   * @param array $config
   */
  public function __construct(array $config = [], string $prefix = '')
  {

    $this->_prefix = $prefix;

    $this->inst = new Client(array_merge([
      'host' => '127.0.0.1',
      'port' => 6379,
      'connections' => [
        'tcp'  => 'Predis\Connection\PhpiredisStreamConnection',
        'unix' => 'Predis\Connection\PhpiredisSocketConnection',
      ]
    ], $config));

  }


  /**
   * Fetches a value from the cache.
   *
   * @param string $key The unique key of this item in the cache.
   * @param mixed $default Default value to return if the key does not exist.
   *
   * @return mixed The value of the item from the cache, or $default in case of cache miss.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if the $key string is not a legal value.
   */
  public function get($key, $default = null)
  {
    $val = $this->inst->get($this->_prefix . $key);
    return !!$val ? unserialize($val) : $default;
  }


  /**
   * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
   *
   * @param string $key The key of the item to store.
   * @param mixed $value The value of the item to store, must be serializable.
   * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
   *                                      the driver supports TTL then the library may set a default value
   *                                      for it or let the driver take care of that.
   *
   * @return bool True on success and false on failure.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if the $key string is not a legal value.
   */
  public function set($key, $value, $ttl = null)
  {

    if($ttl instanceof \DateInterval) $ttl = Util::convertDateIntervalToSeconds($ttl);

    // Set single value to memory
    if(!!$ttl) {
      return $this->inst->set($this->_prefix . $key, serialize($value), 'EX', $ttl) == self::STATUS_OK;
    } else {
      return $this->inst->set($this->_prefix . $key, serialize($value)) == self::STATUS_OK;
    }

  }


  /**
   * Delete an item from the cache by its unique key.
   *
   * @param string $key The unique cache key of the item to delete.
   *
   * @return bool True if the item was successfully removed. False if there was an error.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if the $key string is not a legal value.
   */
  public function delete($key)
  {
    return $this->inst->del($this->_prefix . $key) == 1;
  }


  /**
   * Wipes clean the entire cache's keys.
   *
   * @return bool True on success and false on failure.
   */
  public function clear()
  {
    return $this->inst->flushall() == self::STATUS_OK;
  }


  /**
   * Obtains multiple cache items by their unique keys.
   *
   * @param iterable $keys A list of keys that can obtained in a single operation.
   * @param mixed $default Default value to return for keys that do not exist.
   *
   * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if $keys is neither an array nor a Traversable,
   *   or if any of the $keys are not a legal value.
   */
  public function getMultiple($keys, $default = null)
  {

    // Make sure list of keys is iterable
    if(!is_iterable($keys)) throw new InvalidKeyTypeArgumentException();
    $mappedKeys = Util::convertIterableToIndexArray($keys, $this->_prefix);

    // Get all values in array of keys
    $values = $this->inst->mget($mappedKeys);

    // Set default for all null values if default is present
    $values = array_map(function($value) use($default) { return !!$value ? unserialize($value) : $default; }, $values);

    // Combines keys and values
    $data = array_combine($keys, $values);

    // Return data or empty array if data invalid
    return $data ?: [];

  }


  /**
   * Persists a set of key => value pairs in the cache, with an optional TTL.
   *
   * @param iterable $values A list of key => value pairs for a multiple-set operation.
   * @param null|int|\DateInterval $ttl Optional. The TTL value of this item. If no value is sent and
   *                                       the driver supports TTL then the library may set a default value
   *                                       for it or let the driver take care of that.
   *
   * @return bool True on success and false on failure.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if $values is neither an array nor a Traversable,
   *   or if any of the $values are not a legal value.
   */
  public function setMultiple($values, $ttl = null)
  {

    // Make sure values argument is traverseble
    if(!is_iterable($values) && !!@$values[0]) throw new InvalidDictionaryArgumentException();

    // Dict iterable to array
    $data = Util::convertIterableToAssocArray($values, $this->_prefix);
    foreach ($values as $key => $val) $data[$this->_prefix . $key] = serialize($val);

    // Cache multiple items
    $status = $this->inst->mset($data);

    /* Set TTL for cache items if it has been specified */
    if(!!$ttl) {

      if($ttl instanceof \DateInterval) $ttl = Util::convertDateIntervalToSeconds($ttl);
      foreach ($values as $key => $val) $this->inst->expire($this->_prefix . $key, $ttl);

    }

    return !!$status ? strtoupper($status) == self::STATUS_OK : false;

  }


  /**
   * Deletes multiple cache items in a single operation.
   *
   * @param iterable $keys A list of string-based keys to be deleted.
   *
   * @return bool True if the items were successfully removed. False if there was an error.
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if $keys is neither an array nor a Traversable,
   *   or if any of the $keys are not a legal value.
   */
  public function deleteMultiple($keys)
  {

    if(!is_iterable($keys)) throw new InvalidKeyTypeArgumentException();

    $keys = Util::convertIterableToIndexArray($keys);
    return $this->inst->del($keys) == count($keys);

  }


  /**
   * Determines whether an item is present in the cache.
   *
   * NOTE: It is recommended that has() is only to be used for cache warming type purposes
   * and not to be used within your live applications operations for get/set, as this method
   * is subject to a race condition where your has() will return true and immediately after,
   * another script can remove it making the state of your app out of date.
   *
   * @param string $key The cache item key.
   *
   * @return bool
   *
   * @throws \Psr\SimpleCache\InvalidArgumentException
   *   MUST be thrown if the $key string is not a legal value.
   */
  public function has($key)
  {
    return $this->inst->exists($this->_prefix . $key) == 1;
  }

}
