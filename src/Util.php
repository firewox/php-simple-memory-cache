<?php


namespace Firewox\SimpleMemoryCache;


class Util
{

  public static function convertDateIntervalToSeconds(\DateInterval $interval) : int {

    $days = $interval->format('%a');
    $seconds = 0;

    if($days){
      $seconds += 24 * 60 * 60 * $days;
    }

    $hours = $interval->format('%H');
    if($hours){
      $seconds += 60 * 60 * $hours;
    }

    $minutes = $interval->format('%i');
    if($minutes){
      $seconds += 60 * $minutes;
    }

    $seconds += (int) $interval->format('%s');

    return $seconds;

  }


  public static function convertIterableToAssocArray($iterable, $prefix)
  {

    $data = [];
    foreach ($iterable as $key => $val) {
      $data[$prefix . $key] = $val;
    }

    return $data;

  }


  public static function convertIterableToIndexArray($iterable, $prefix)
  {

    $data = [];
    foreach ($iterable as $key => $val) {
      $data[] = $prefix . $val;
    }

    return $data;

  }

}
