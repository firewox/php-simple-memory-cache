<?php


namespace Firewox\SimpleMemoryCache\Exceptions;


use Psr\SimpleCache\InvalidArgumentException;

class InvalidKeyTypeArgumentException extends \Exception implements InvalidArgumentException
{


  /**
   * InvalidArgument constructor.
   */
  public function __construct()
  {
    parent::__construct('Invalid key type specified.');
  }

}
