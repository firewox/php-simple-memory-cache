<?php


namespace Firewox\SimpleMemoryCache\Exceptions;


use Psr\SimpleCache\InvalidArgumentException;

class InvalidDictionaryArgumentException extends \Exception implements InvalidArgumentException
{


  /**
   * InvalidArgument constructor.
   */
  public function __construct()
  {
    parent::__construct('Invalid argument specified. Argument must be iterable dictionary.');
  }

}
