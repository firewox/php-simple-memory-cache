<?php

namespace FirewoxTests;

use Firewox\SimpleMemoryCache\Redis;
use PHPUnit\Framework\TestCase;

class RedisTest extends TestCase
{

  public function testRedisConstructor() {

    $redis = new Redis();
    $this->assertInstanceOf(Redis::class, $redis);

  }


  public function testRedisSetOne() {

    $redis = new Redis();
    $this->assertSame(true, $redis->set('test', 'hello world', 300));

  }


  public function testRedisGetOne() {

    $redis = new Redis();
    $this->assertSame('hello world', $redis->get('test'));

  }


  public function testRedisSetMultiple() {

    $redis = new Redis();
    $this->assertSame(true, $redis->setMultiple(['test1' => 'hello','test2' => 'world'], 300));

  }


  public function testRedisGetMultiple() {

    $redis = new Redis();
    $this->assertSame(['test1' => 'hello','test2' => 'world'], $redis->getMultiple(['test1', 'test2']));

  }


  public function testRedisDeleteOne() {

    $redis = new Redis();
    $this->assertSame(true, $redis->delete('test'));

  }


  public function testRedisDeleteMultiple() {

    $redis = new Redis();
    $this->assertSame(true, $redis->deleteMultiple(['test1', 'test2']));

  }


  public function testRedisClear() {

    $redis = new Redis();
    $this->assertSame(true, $redis->clear());

  }

}
