# PHP Simple Memory Cache

Library for supporting any kind of in memory caching server using PSR-16 simple cache standard.

## Getting Started

Use composer package manager to install Simple Memory Cache.

```bash
composer require firewox/simple-memory-cache
```

### Prerequisites & configurations

This library requires at least PHP 7.1 to run. In your PHP file 
include composer autoload:

```php
require_once 'vendor/autoload.php';
```

### Structuring your code

Simple memory cache library attempts to abstract all the known 
in-memory cache server methods using a common interface. This
library implements all the methods as defined in PSR 16 standard
(https://www.php-fig.org/psr/psr-16/).

1. Cache one item to memory using Redis

```php
$redis = new Redis();
$status = $redis->set('test', 'hello world', 300);
```

2. Get one item cached in memory using Redis

```php
$redis = new Redis();
$value = $redis->get('test');
```

3. Delete one item cached in memory using Redis

```php
$redis = new Redis();
$value = $redis->delete('test');
```

4. Cache multiple items to memory using Redis

```php
$redis = new Redis();
$status = $redis->setMultiple(['test1' => 'hello', 'test2' => 'world'], 300);
```

5. Get multiple items cached in memory using Redis

```php
$redis = new Redis();
$values = $redis->getMultiple(['test1' => 'hello', 'test2' => 'world']);
```

6. Delete multiple items cached in memory using Redis

```php
$redis = new Redis();
$value = $redis->deleteMultiple(['test1', 'test2']);
```
